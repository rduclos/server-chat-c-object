#include "Server.hpp"

int 	main()
{
	int		port;

	std::cout << "Veuillez entrer un port entre 1024 et 49151 : " << std::endl;
	std::cin >> port;
	if (port < 1024 || port > 49151)
	{
		std::cerr << "Server : error wrong port." << std::endl;
		exit (0);
	}
	Server my_serv(port);
	S_Client **clients = NULL;
	my_serv.init_clt(clients);
	std::cout << "Server started." << std::endl;
	while (my_serv.run_serv() == 1)
		;
	my_serv.stop_serv();
	return (0);
}
