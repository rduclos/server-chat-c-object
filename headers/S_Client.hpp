#ifndef S_CLIENT_HPP
# define S_CLIENT_HPP

# include <iostream>
# include <cstdlib>
# include <unistd.h>
# include <strings.h>
# include <cstring>
# include <sys/types.h>//pour les sockets
# include <sys/socket.h>//pour les sockets
# include <netinet/in.h>//pour les sockets
# include <arpa/inet.h>//pour les sockets

enum types {SRV_FD, CLT_FD, FREE_FD};

class S_Client
{

	private:
	int				sock;
	int			  	type;
	std::string  	name;
	buf_circle		b_read;
	buf_circle		b_write;
	char			tmp_read[BC_SIZE + 1];
	char			tmp_write[BC_SIZE + 1];
	
	public:
	S_Client(int me);
	~S_Client(void);
	void			c_send(void);
	int				is_write(void);
	char			*c_receive(void);
	void			clear_tmp_read(void);
	void			set_name(std::string me);
	std::string		get_name(void);
	int				is_named(void);
	void			set_type(int me);
	int				get_type(void);
	void			set_write(char *tmp);
	char			*get_read(void);
};

#endif
